<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VendorCategory;
use Validator;

class VendorCategoryController extends Controller
{
    private $model;
    private $vendorData;

	public function __construct(){	
        $this->middleware('permission:vendor-category-list|vendor-category-create|vendor-category-edit|vendor-category-delete', ['only' => ['index','show']]);
        $this->middleware('permission:vendor-category-create', ['only' => ['create','store']]);
        $this->middleware('permission:vendor-category-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:vendor-category-delete', ['only' => ['destroy']]);
		$this->model = VendorCategory::class; /* url evave rakhben na ..route use korben name dia */
	}

    /* Layout evave render koraben na... func r viotre return korben directly
    compact kore data pass kore diben .. 
    */

    public function index(Request $request)
    {
		$allData =  $this->model::orderBy('id', 'desc')->get();
        return view('backend.vendorCategories.index', compact('allData'));
    }

    public function create()
    {
        $objData = '';
        return view('backend.vendorCategories.create', compact('objData'));
    }

    public function edit($id)
    {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		if( !$id ){ exit('Bad Request!'); }

        $objData = $this->model::where('id', $id)->first();

		return view('backend.vendorCategories.create', compact('objData'));
    }

    public function store(Request $request)
    {
        $id = $request['id'];

        $rules = [
            'description'        => 'required',
            'status'        => 'required',
        ];

        $attribute =[
            'description'      => 'Description',
            'status'      => 'Status',
        ];

        $customMessages =[];

        $validator = Validator::make($request->all(), $rules, $customMessages, $attribute);

        if ($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput();
        }  

		$this->vendorData = [ 
            'description' => $request['description'],
            'status' => $request['status'],
        ]; 

        if ( empty($id) ){
			// Insert Query
            $this->model::create($this->vendorData);

			return redirect()->route('system.vendor-category')->with('success', 'Record Successfully Created.');

        }else{
            // Update Query
            $this->model::where('id', $id)->update($this->vendorData);

            return redirect()->route('system.vendor-category')->with('success', 'Successfully Updated');
        }
    }

    public function show($id)
    {		
		$id = filter_var($id, FILTER_VALIDATE_INT);
		if( !$id ){ exit('Bad Request!'); }

        $objData = $this->model::where('id', $id)->first();		       

		return view('backend.vendorCategories.view', compact('objData'));

    }

    public function destroy(Request $request, $id)
    {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		if( !$id ){ exit('Bad Request!'); }

        $objData = $this->model::where('id', $id)->first();
        $pageUrl = 'system/vendor-category/delete/'.$id;

		if($request->method() === 'POST' ){
			
            $vendor = $objData->description;

            $this->model::where('id',$id)->delete();

			echo json_encode(['fail' => FALSE, 'error_messages' => "Vendor ".$vendor." was deleted."]);
		}else{
            return view('backend.vendorCategories.delete', compact('objData', 'pageUrl'));
		}
    }
}
