<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VendorTournament;
use App\Models\VendorCategory;
use Validator;

class VendorTournamentController extends Controller
{
    private $model;

	public function __construct(){	

        $this->middleware('permission:vendor-tournament-list|vendor-tournament-create|vendor-tournament-edit|vendor-tournament-delete', ['only' => ['index','show']]);
        $this->middleware('permission:vendor-tournament-create', ['only' => ['create','store']]);
        $this->middleware('permission:vendor-tournament-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:vendor-tournament-delete', ['only' => ['destroy']]);
		$this->model = VendorTournament::class;
	}

    public function index()
    {
        $vendorCategories = VendorCategory::all();
		$allData =  $this->model::orderBy('tournaments_id', 'desc')->get();
        return view('backend.vendorTournaments.index', compact('allData', 'vendorCategories'));
    }

    public function store(Request $request)
    {
        $rules = [
            'tournament_name'        => 'required|string',
            'sport_type'        => 'required',
        ];

        $attribute =[
            'tournament_name'      => 'Tournament Title',
            'sport_type'      => 'Sport Type',
        ];

        $customMessages =[];

        $validator = Validator::make($request->all(), $rules, $customMessages, $attribute);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $tourData = [
            'tournament_name' => $request['tournament_name'],
            'sport_type' => $request['sport_type'],
        ];

        $this->model::create($tourData);

        return redirect()->route('vendor_tournament.index')->with('success', 'Record Successfully Created!');

    }

    public function edit($id)
    {
        $objData = $this->model::where('tournaments_id', $id)->first();
        $vendorCategories = VendorCategory::all();
        return view('backend.vendorTournaments.edit', compact('vendorCategories', 'objData'));
    }
    
    public function update(Request $request)
    {
        $id = $request['id'];

        $rules = [
            'tournament_name'        => 'required|string',
            'sport_type'        => 'required',
        ];

        $attribute =[
            'tournament_name'      => 'Tournament Title',
            'sport_type'      => 'Sport Type',
        ];

        $customMessages =[];

        $validator = Validator::make($request->all(), $rules, $customMessages, $attribute);

        if ($validator->fails()){
            return response()->json($validator->messages(), 200);
        }

        $tourData = [
            'tournament_name' => $request['tournament_name'],
            'sport_type' => $request['sport_type'],
        ];

        $this->model::where('tournaments_id', $id)->update($tourData);

        return 'success';
    }

    public function destroy(Request $request, $id)
    {
        $id = filter_var($id, FILTER_VALIDATE_INT);
        if( !$id ){ exit('Bad Request!'); }

        $pageUrl = 'system/vendor-tournament/delete/'.$id;
        $objData = $this->model::where('tournaments_id', $id)->first();

        if($request->method() === 'POST' ){

            $title = $objData->tournament_name;

            $this->model::where('tournaments_id', $id)->delete();

            return json_encode(['fail' => FALSE, 'error_messages' => $title." Tournament was deleted."]);
        }else{
            return view('backend.vendorTournaments.delete', compact('pageUrl', 'objData'));
        }

    }
}
