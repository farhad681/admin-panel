<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vendor;
use App\Models\VendorCategory;
use App\Models\VendorTournament;
use Validator;
use DB;

class VendorController extends Controller
{
    private $model;
	private $data;
	private $tableId;
	private $bUrl;
    private $vendorData;

	public function __construct(){	
        $this->middleware('permission:vendor-list|vendor-create|vendor-edit|vendor-delete', ['only' => ['index']]);
        $this->middleware('permission:vendor-create', ['only' => ['create','store']]);
        $this->middleware('permission:vendor-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:vendor-delete', ['only' => ['destroy']]);
		$this->tableId = 'vendor_id';
		$this->model = Vendor::class;
		$this->bUrl = '/system/vendor';
	}


	public function layout($pageName){
		$this->data['tableID'] = $this->tableId;
		$this->data['bUrl'] = $this->bUrl;		
		echo view('backend.vendors.'.$pageName.'', $this->data);
	}

    public function index($id = null)
    {
        $this->data = [
            'title'         => 'Vendor',
			'pageUrl'         => $this->bUrl,
            'page_icon'     => '<i class="fa fa-book"></i>',
            'vendorCategories' => VendorCategory::all(),
            'category' => $id,
        ];

        if($id != null || $id != "") {
            $this->data['category'] = $id;
            $this->data['vendors'] = DB::table('vendor_all')
                    ->join('vendor_category', 'vendor_category.id', '=', 'vendor_all.cat_id')
                    ->join('vendor_tournaments', 'vendor_tournaments.tournaments_id', '=', 'vendor_all.tournament')
                    ->where('vendor_all.cat_id', $id)
                    ->select('vendor_all.*', 'vendor_tournaments.tournament_name')
                    ->get();
            $this->layout('index');
        } 
        else {
            // $this->data['allData'] =  $this->model::where($this->tableId, $request->vendor_id)->orderBy('vendor_id', 'desc')->get();
            $this->data['category'] = 2;
            $this->data['vendors'] = DB::table('vendor_all')
                    ->join('vendor_category', 'vendor_category.id', '=', 'vendor_all.cat_id')
                    ->join('vendor_tournaments', 'vendor_tournaments.tournaments_id', '=', 'vendor_all.tournament')
                    ->where('vendor_all.cat_id', 2)
                    ->select('vendor_all.*', 'vendor_tournaments.tournament_name')
                    ->get();
            $this->layout('index');
        }	

    }

    public function create()
    {
        $this->data = [
            'title'         => 'Add New Vendor',
            'page_icon'     => '<i class="fa fa-plus-circle"></i>',
            'vendorCategories' => VendorCategory::all(),
            'vendorTorunaments' => VendorTournament::all(),
            'objData'       => '',
        ];

		$this->layout('create');
    }

    public function edit($id)
    {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		if( !$id ){ exit('Bad Request!'); }

        $this->data = [
            'title'         => 'Edit Vendor',
			'page_icon'     => '<i class="fa fa-edit"></i>',
            'vendorCategories' => VendorCategory::all(),
            'vendorTorunaments' => VendorTournament::all(),
            'objData'       => $this->model::where($this->tableId, $id)->first(),
        ];

		$this->layout('create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $id = $request[$this->tableId];

        $rules = [
            'cat_id'        => 'required',
            'tournament'        => 'required',
            // 'vendor_title'        => 'required',
            'first_team'        => 'required',
            'second_team'        => 'required',
            'match_date'        => 'required',
            'match_time'        => 'required',
        ];

        $attribute =[
            'cat_id'        => 'Catebory',
            'tournament'        => 'Tournament',
            // 'vendor_title'        => 'Title',
            'first_team'        => 'First Team',
            'second_team'        => 'Second Team',
            'match_date'        => 'Date',
            'match_time'        => 'Time',
        ];

        $customMessages =[];

        $validator = Validator::make($request->all(), $rules, $customMessages, $attribute);

        if ($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput();
        }  

		$this->vendorData = [ 
            'cat_id'        => $request['cat_id'],
            'tournament'        => $request['tournament'],
            'vendor_title'        => $request['vendor_title'],
            'first_team'        => $request['first_team'],
            'second_team'        => $request['second_team'],
            'vendor_datetime'        => $request['match_date']." ".$request['match_time'],
            'status'        => 1,
        ]; 

        if ( empty($id) ){
			// Insert Query
            $this->model::create($this->vendorData);

			return redirect($this->bUrl)->with('success', 'Record Successfully Created.');

        }else{
            // Update Query
            $this->model::where($this->tableId, $id)->update($this->vendorData);

            return redirect($this->bUrl)->with('success', 'Successfully Updated');
        }
    }

    public function show($id)
    {		
		$id = filter_var($id, FILTER_VALIDATE_INT);
		if( !$id ){ exit('Bad Request!'); }

        $this->data = [
            'title'         => 'Vendor Information',
			'page_icon'     => '<i class="fa fa-eye"></i>',
        ];		

        $this->data['objData'] = DB::table('vendor_all')
                                ->join('vendor_category', 'vendor_category.id', '=', 'vendor_all.cat_id')
                                ->join('vendor_tournaments', 'vendor_tournaments.tournaments_id', '=', 'vendor_all.tournament')
                                ->where('vendor_all.vendor_id', $id)
                                ->select('vendor_all.*', 'vendor_tournaments.tournament_name', 'vendor_category.description')
                                ->first();

		$this->layout('view');

    }

    public function destroy(Request $request, $id)
    {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		if( !$id ){ exit('Bad Request!'); }

        $this->data = [
            'title'     => 'Delete Vendor',
            'pageUrl'   => $this->bUrl.'/delete/'.$id,
			'page_icon' => '<i class="fa fa-book"></i>',
            'objData'   => $this->model::where($this->tableId, $id)->first(),
        ];

		$this->data['tableID'] = $this->tableId;
		$this->data['bUrl'] = $this->bUrl;

		if($request->method() === 'POST' ){
			
            $vendor = $this->data['objData']->description;

            $this->model::where('vendor_id', $id)->delete();

			echo json_encode(['fail' => FALSE, 'error_messages' => "Vendor was deleted."]);
		}else{
			$this->layout('delete');
		}
    }
}
