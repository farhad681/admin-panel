<?php


/************
 * **
 * App Helper 
 * Version 1.0
 * @author - TOSHOST
 * Basic functions for the App
 * **
 */



/*****
 * validation_errors()
 * formating validation errors for views
 **/

function validation_errors($errors){
    if($errors->any()){
        echo '<div class="alert alert-danger"><ul>';
        foreach ($errors->all() as $error){
            echo '<li>'.$error.'</li>';
        }
        echo '</ul></div>';
    }
}

function getValue($field, $data, $default=null){
    return (!empty($data) && !empty($data->$field)) ? $data->$field : old($field,$default);
}