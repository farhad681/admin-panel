<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorTournament extends Model
{
    use HasFactory;

    protected $table = 'vendor_tournaments';

    protected $fillable = [
        'sport_type', 'tournament_name'
    ];
}
