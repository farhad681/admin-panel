<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;
    protected $table = 'vendor_all';

    protected $fillable = [
        'cat_id', 'tournament', 'vendor_title', 'first_team', 'second_team', 'vendor_datetime', 'status'
    ];
}
