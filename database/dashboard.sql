-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2021 at 03:14 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dashboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'category 1', 'this its testing', '2021-09-02 00:27:28', '2021-09-02 00:28:39'),
(2, 'category 2', 'this its testing', '2021-09-02 00:27:46', '2021-09-02 00:27:46');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_09_135640_create_permission_tables', 1),
(5, '2020_10_09_135732_create_products_table', 1),
(6, '2021_09_02_061343_create_categories_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2),
(3, 'App\\Models\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(2, 'role-create', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(3, 'role-edit', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(4, 'role-delete', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(5, 'product-list', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(6, 'product-create', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(7, 'product-edit', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(8, 'product-delete', 'web', '2021-09-01 00:25:45', '2021-09-01 00:25:45'),
(9, 'category-list', 'web', NULL, NULL),
(10, 'category-create', 'web', NULL, NULL),
(11, 'category-edit', 'web', NULL, NULL),
(12, 'category-delete', 'web', NULL, NULL),
(13, 'vendor-category-list', 'web', NULL, NULL),
(14, 'vendor-category-create', 'web', NULL, NULL),
(15, 'vendor-category-edit', 'web', NULL, NULL),
(16, 'vendor-category-delete', 'web', NULL, NULL),
(17, 'vendor-tournament-list', 'web', NULL, NULL),
(18, 'vendor-tournament-create', 'web', NULL, NULL),
(19, 'vendor-tournament-edit', 'web', NULL, NULL),
(20, 'vendor-tournament-delete', 'web', NULL, NULL),
(21, 'vendor-list', 'web', NULL, NULL),
(22, 'vendor-create', 'web', NULL, NULL),
(23, 'vendor-edit', 'web', NULL, NULL),
(24, 'vendor-delete', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'test updated', 'updated description', '2021-09-01 05:44:42', '2021-09-01 06:24:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2021-09-01 00:26:50', '2021-09-01 00:26:50'),
(2, 'User', 'web', '2021-09-01 00:31:37', '2021-09-01 00:31:37'),
(3, 'Manager', 'web', '2021-09-01 04:32:56', '2021-09-01 04:32:56');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hardik Savani', 'admin@gmail.com', NULL, '$2y$10$hJhW/F8EMXz6E9MYur6.jer/77WMm5trRm.3WGtl1eXGiy8U8pmYu', NULL, '2021-09-01 00:26:50', '2021-09-01 00:26:50'),
(2, 'Mezbahul Islam Milu', 'milu@gmail.com', NULL, '$2y$10$10hhLIIteKuuUJ1zTznB3Onuy6TDy.DDsR0pkqrrCfxZfvWGs75s6', NULL, '2021-09-01 00:33:04', '2021-09-01 00:33:04'),
(3, 'Nadim Saheb', 'nadim@gmail.com', NULL, '$2y$10$gUJHnZu3FNGP9nShgJxT/eodzcJwcKjqfkTw7EACi8PwKMMohCpTG', NULL, '2021-09-01 00:41:40', '2021-09-02 00:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_all`
--

CREATE TABLE `vendor_all` (
  `vendor_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `tournament` varchar(255) DEFAULT NULL,
  `vendor_title` varchar(255) DEFAULT NULL,
  `first_team` varchar(255) DEFAULT NULL,
  `second_team` varchar(255) DEFAULT NULL,
  `vendor_datetime` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_all`
--

INSERT INTO `vendor_all` (`vendor_id`, `cat_id`, `tournament`, `vendor_title`, `first_team`, `second_team`, `vendor_datetime`, `status`, `created_at`, `updated_at`) VALUES
(546304, 1, '1', NULL, 'Jocoro', 'Metapan update', '2021-09-05 14:00', '1', '2021-08-15 05:26:11', '2021-09-05 08:20:15'),
(554240, 2, '2', '', 'Pardubice B', 'Zivanice', '2021-08-15 10:15', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(502016, 3, '3', '', 'Rosario Central', 'Independiente', '2021-08-15 01:15', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(544513, 5, '1', '', 'Balassagyarmat', 'Dabas-Gyon', '2021-08-15 17:30', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(546305, 4, '2', '', 'Municipal Limeno', 'FAS', '2021-08-15 02:30', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(554241, 2, '3', '', 'Zapy', 'Zbuzany', '2021-08-15 17:00', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(468226, 3, '3', '', 'Umea FC', 'Storfors AIK', '2021-08-15 15:00', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(544514, 4, '1', '', 'Cegledi', 'Mohacsi', '2021-08-15 17:30', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(546306, 5, '2', '', 'Platense Municipal', '11 Deportivo', '2021-08-15 23:15', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(554242, 1, '3', '', 'Cesky Brod B', 'Kosor', '2021-08-15 17:00', '0', '2021-08-15 05:26:11', '2021-08-15 05:26:11'),
(564810, 1, '2', NULL, 'Bangaldesh', 'Newzealand', '2021-09-05 14:00', '1', '2021-09-05 07:53:32', '2021-09-05 07:53:32');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category`
--

CREATE TABLE `vendor_category` (
  `id` int(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `updated_at` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_category`
--

INSERT INTO `vendor_category` (`id`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'cricket', 1, '16-09-2019 04:38:45', '16-09-2019 04:38:45'),
(2, 'football', 1, '16-09-2019 04:38:45', '16-09-2019 04:38:45'),
(3, 'basketball', 1, '16-09-2019 04:38:45', '16-09-2019 04:38:45'),
(4, 'volley', 1, '16-09-2019 04:38:45', '16-09-2019 04:38:45'),
(5, 'tennis updated', 1, '16-09-2019 04:38:45', '2021-09-20 12:34:01'),
(7, 'new added', 0, '2021-09-20 12:34:16', '2021-09-20 12:34:16');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_tournaments`
--

CREATE TABLE `vendor_tournaments` (
  `tournaments_id` int(11) NOT NULL,
  `sport_type` varchar(255) NOT NULL,
  `tournament_name` varchar(255) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_tournaments`
--

INSERT INTO `vendor_tournaments` (`tournaments_id`, `sport_type`, `tournament_name`, `created_at`, `updated_at`) VALUES
(1, '2', 'Primera Division', '2021-08-15 05:26:11', NULL),
(2, '1', 'National Division', '2021-08-15 05:26:11', NULL),
(3, '4', 'Liga Primera', '2021-08-15 05:26:11', NULL),
(121354, '2', 'test', '2021-09-20 12:58:13', '2021-09-20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vendor_all`
--
ALTER TABLE `vendor_all`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Indexes for table `vendor_category`
--
ALTER TABLE `vendor_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_tournaments`
--
ALTER TABLE `vendor_tournaments`
  ADD PRIMARY KEY (`tournaments_id`),
  ADD KEY `tournaments_id` (`tournaments_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vendor_all`
--
ALTER TABLE `vendor_all`
  MODIFY `vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=564811;

--
-- AUTO_INCREMENT for table `vendor_category`
--
ALTER TABLE `vendor_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vendor_tournaments`
--
ALTER TABLE `vendor_tournaments`
  MODIFY `tournaments_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121355;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
