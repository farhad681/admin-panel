<?php

use Illuminate\Support\Facades\Route;

// use App\Http\Controllers\HomeController;
// use App\Http\Controllers\Backend\RoleController;
// use App\Http\Controllers\Backend\UserController;
// use App\Http\Controllers\Backend\ProductController;
// use App\Http\Controllers\Backend\IndexController;
// use App\Http\Controllers\Backend\CategoryController;
// use App\Http\Controllers\Backend\GlobalSettingsController;
// use App\Http\Controllers\Backend\VendorCategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth'], 'prefix' => 'system'], function() {
    // Route::get('system/dashboard', 'IndexController@index')->name('system.home');
    Route::get('/dashboard', 'Backend\IndexController@index')->name('system.home');
    Route::resource('/roles', 'Backend\RoleController');
    Route::resource('/users', 'Backend\UserController');
    Route::resource('/products', 'Backend\ProductController');
    Route::resource('/categories', 'Backend\CategoryController');
    Route::resource('/global-settings', 'Backend\GlobalSettingsController');

    //// Vendor Category ////
    Route::resource('/vendor-category', 'Backend\VendorCategoryController',  [
        'names' => [
            'create' => 'system.vendor-category.create',
            'edit' => 'system.vendor-category.edit',
            'show' => 'system.vendor-category.show',
        ]])->only(['create', 'edit', 'show']);

    Route::match(['get', 'post'], '/vendor-category', 'Backend\VendorCategoryController@index')->name('system.vendor-category');
    Route::post('/vendor-category/store', 'Backend\VendorCategoryController@store')->name('system.vendor-category.store');
    Route::match(['get', 'post'], '/vendor-category/delete/{id}', 'Backend\VendorCategoryController@destroy')
        ->where(['id'=>'[0-9]+'])->name('system.vendor-category.delete');

    //// Vendor Tournament ////
    Route::match(['get', 'post'], '/vendor-tournament', 'Backend\VendorTournamentController@index')->name('vendor_tournament.index');
    Route::post('/vendor-tournament/store', 'Backend\VendorTournamentController@store')->name('vendor_tournament.store');
    Route::match(['get', 'post'], '/vendor-tournament/delete/{id}', 'Backend\VendorTournamentController@destroy')
        ->where(['id'=>'[0-9]+'])->name('vendor_tournament.destroy');
    Route::get('/vendor-tournament/{id}/edit', 'Backend\VendorTournamentController@edit')->name('vendor_tournament.edit');
    Route::post('/vendor-tournament/update', 'Backend\VendorTournamentController@update')->name('vendor_tournament.update');

    //// Vendor ////
    // Route::get('/vendor-category/{id}', 'Backend\VendorController@index');
    Route::resource('/vendor', 'Backend\VendorController',  [
        'names' => [
            'create' => 'system.vendor.create',
            'edit' => 'system.vendor.edit',
        ]])->only(['create', 'edit']);

    Route::get('/vendor/{cat_id?}', 'Backend\VendorController@index')->name('system.vendor');
    Route::post('/vendor/store', 'Backend\VendorController@store')->name('system.vendor.store');
    Route::match(['get', 'post'], '/vendor/delete/{id}', 'Backend\VendorController@destroy')
        ->where(['id'=>'[0-9]+'])->name('system.vendor.delete');

});