@extends("backend.master")
@section("content")
<!-- Main content -->
<section class="container-fluid">

  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> User Information </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool">
          <a href="{{url('system/users/'.$user->id.'/edit')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i>
            <i class="fa fa-edit"></i> Edit</a>
          <a href="{{url('system/users')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i
              class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>

    <div class="card-body">
      <div class="col-md-11">
        <div class="row">
          <label class="col-sm-3">Name</label>
          <div class="col-sm-9">
            {{ $user->name }}
          </div>
        </div>

        <div class="row">
          <label class="col-sm-3">Email</label>
          <div class="col-sm-9">
            {{ $user->email }}
          </div>
        </div>

        <div class="row">
          <label class="col-sm-3">Roles</label>
          <div class="col-sm-9">
            @if(!empty($user->getRoleNames()))
            @foreach($user->getRoleNames() as $v)
            {{ $v }}
            @endforeach
            @endif
          </div>
        </div>

      </div>
    </div>
  </div>
  </div>
</section>
@endsection