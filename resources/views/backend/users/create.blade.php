@extends('backend.master')

@section('content')
<!-- Main content -->
<section class="container-fluid">
  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> Add User </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool" >
          <a href="{{url('system/users')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>

		{!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}

      <div class="card-body">

				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

        <div class="row">
          <div class="col-sm-6">

            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name</label>
              <div class="col-sm-8">
								{!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
              </div>
            </div>

						<div class="form-group row">
              <label class="col-sm-4 col-form-label">Password</label>
              <div class="col-sm-8">
								{!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
              </div>
            </div>

						<div class="form-group row">
              <label class="col-sm-4 col-form-label">Role</label>
              <div class="col-sm-8">
								{!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
              </div>
            </div>

          </div>
          <div class="col-sm-6">

						<div class="form-group row">
              <label class="col-sm-4 col-form-label">Email</label>
              <div class="col-sm-8">
								{!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
              </div>
            </div>

						<div class="form-group row">
              <label class="col-sm-4 col-form-label">Confirm Password</label>
              <div class="col-sm-8">
								{!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
              </div>
            </div>

          </div>
        </div>
      </div>

      <div class="card-footer">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group row">
              <div class="col-sm-4"></div>
              <div class="col-sm-8">
                <button type="submit" class="btn btn-primary">Save</button>&nbsp;&nbsp;
                <a href="{{url('system/users')}}" class="btn btn-warning">Cancel</a>
              </div> 
            </div>
          </div>
        </div>
      </div>

		{!! Form::close() !!}
  </div>
</section>
<!-- /.content -->
@endsection