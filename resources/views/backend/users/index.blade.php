@push('css')
  <style>
    input.form-control.float-left.search_input{
      width: 250px;
    }
    ul.pagination{
      float: right;
    }
  </style>
@endpush
@extends("backend.master")
@section("content")

  <section class="content">
    <!-- Default box -->
    <div class="row">

      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h2 class="card-title"> User List </h2>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>

              <button type="button" class="btn btn-tool" >
                <a href="{{url('system/users/create')}}" class="btn bg-gradient-info custom_btn"><i class="mdi mdi-plus"></i> <i class="fa fa-plus-circle"></i> Add New </a>
              </button>

            </div>
          </div>
          <div class="card-body frontoffice-body">
            <table class="table table-bordered">
              <thead>
              <tr>
                <th class="text-center" width="5%">SL</th>
                <th class="sort" data-row="name" id="name" width="20%">Name</th>
                <th class="text-center" width="10%">Email</th>
                <th class="" width="20%">Roles</th>
                <th class="text-center" width="20%">Manage</th>
              </tr>
              </thead>
              <tbody>
              @if ($data->count() > 0)

                @php
                  $c = 1;
                @endphp

                @foreach ($data as $key => $user)
                <tr>
                  <td class="text-center" width="5%">{{ ++$key }}</td>
                  <td class="pl-1" width="20%">{{ $user->name }}</td>
                  <td class="text-center" width="10%">{{ $user->email }}</td>
                  <td class="pl-1" width="20%">
                    @if(!empty($user->getRoleNames()))
                      @foreach($user->getRoleNames() as $v)
                        <label class="badge badge-success">{{ $v }}</label>
                      @endforeach
                    @endif
                  </td>

                  <td class="text-center" width="20%">
                    <div class="btn-group">

                      <button type="button" class="btn btn-outline-info">
                        <a href="{{ route('users.show',$user->id) }}"><i class="fa fa-table"></i> </a>
                      </button>

                      <button type="button" class="btn btn-outline-info dropdown-toggle dropdown-hover dropdown-icon"
                        data-toggle="dropdown">
                      </button>
                      <div class="dropdown-menu" role="menu" style="">
                        <a class="dropdown-item" href="{{ route('users.edit',$user->id) }}"><i
                            class="fa fa-edit"></i> Edit</a>

                        <div class="dropdown-divider"></div>

                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) !!}
                        {!! Form::close() !!}

                      </div>
                    </td>
                  </tr>

                  @php
                    $c++;
                  @endphp

                @endforeach

              @else

                <tr>
                  <td colspan="5">There is nothing found.</td>
                </tr>

              @endif
              </tbody>
            </table>
            <div class="row mt-4">

              <div class="col-md-9">
                <div class="pagination_table">
                  {!! $data->render() !!}
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">

          </div>
        </div>

      </div>
    </div>
  </section>
@endsection