@extends("backend.master")
@section("content")
<!-- Main content -->
<section class="container-fluid">

  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> User Information </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool">
          @can('role-edit')
          <a href="{{url('system/roles/'.$role->id.'/edit')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i>
            <i class="fa fa-edit"></i> Edit</a>
          @endcan 
          <a href="{{url('system/roles')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i
              class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>

    <div class="card-body">
      <div class="col-md-11">
        <div class="row">
          <label class="col-sm-3">Name</label>
          <div class="col-sm-9">
            {{ $role->name }}
          </div>
        </div>

        <div class="row">
          <label class="col-sm-3">Permissions</label>
          <div class="col-sm-9">
            @if(!empty($rolePermissions))
            @foreach($rolePermissions as $v)
            {{ $v->name }},
            @endforeach
            @endif
          </div>
        </div>

      </div>
    </div>
  </div>
  </div>
</section>
@endsection