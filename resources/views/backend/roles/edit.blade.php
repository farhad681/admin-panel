@extends('backend.master')

@section('content')
<!-- Main content -->
<section class="container-fluid">
  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> Edit User </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool">
          <a href="{{url('system/roles')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i
              class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>

    {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}

    <div class="card-body">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="row">
        <div class="col-sm-6">

          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Name</label>
            <div class="col-sm-8">
              {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Permission</label>
            <div class="col-sm-8">
              @foreach($permission as $value)
              <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                {{ $value->name }}</label>
              <br />
              @endforeach
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="card-footer">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group row">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <button type="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;
              <a href="{{url('system/roles')}}" class="btn btn-warning">Cancel</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    {!! Form::close() !!}
  </div>
</section>
<!-- /.content -->
@endsection