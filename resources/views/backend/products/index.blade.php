@extends("backend.master")
@section("content")

<section class="content">
  <!-- Default box -->
  <div class="row">

    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h2 class="card-title"> Role List </h2>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>

            @can('product-create')
            <button type="button" class="btn btn-tool">
              <a href="{{url('system/products/create')}}" class="btn bg-gradient-info custom_btn"><i class="mdi mdi-plus"></i>
                <i class="fa fa-plus-circle"></i> Add New </a>
            </button>
            @endcan 

          </div>
        </div>
        <div class="card-body frontoffice-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" width="5%">SL</th>
                <th class="sort" data-row="name" id="name" width="20%">Title</th>
                <th width="20%">Details</th>
                <th class="text-center" width="20%">Manage</th>
              </tr>
            </thead>
            <tbody>
              @if ($products->count() > 0)

              @foreach ($products as $key => $product)
              <tr>
                <td class="text-center" width="5%">{{ ++$key }}</td>
                <td class="pl-1" width="20%">{{ $product->name }}</td>
                <td class="pl-1" width="20%">{{ $product->detail }}</td>

                <td class="text-center" width="20%">
                  <div class="btn-group">

                    <button type="button" class="btn btn-outline-info">
                      <a href="{{ route('products.show',$product->id) }}"><i class="fa fa-table"></i> </a>
                    </button>

                    <button type="button" class="btn btn-outline-info dropdown-toggle dropdown-hover dropdown-icon"
                      data-toggle="dropdown">
                    </button>
                    <div class="dropdown-menu" role="menu" style="">

                      @can('product-edit')
                      <a class="dropdown-item" href="{{ route('products.edit',$product->id) }}"><i
                          class="fa fa-edit"></i>
                        Edit</a>
                      @endcan

                      <div class="dropdown-divider"></div>
                      @can('product-delete')
                      <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                      @endcan

                    </div>
                  </div>
                </td>
              </tr>

              @endforeach

              @else

              <tr>
                <td colspan="5">There is nothing found.</td>
              </tr>


              @endif
            </tbody>
          </table>
          <div class="row mt-4">

            <div class="col-md-9">
              <div class="pagination_table">
                {!! $products->links() !!}
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">

        </div>
      </div>

    </div>
  </div>
</section>

@endsection