@extends('backend.master')

@section('content')
<!-- Main content -->
<section class="container-fluid">
  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> Edit Product </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool">
          <a href="{{url('system/products')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i
              class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>


    <form method="post" action="{{ route('products.update',$product->id) }}" enctype="multipart/form-data">
      @method('put')
      @csrf

      <div class="card-body">

        @if ($errors->any())
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif




        <div class="form-group row" id="area_charge">
          <label class="col-sm-4 col-form-label" for="charge">Title</label>
          <div class="col-sm-8">
            <input name="name" id="name" type="text" class="form-control" value="{{ $product->name }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label" for="detail">Details </label>
          <div class="col-sm-8">
            <textarea name="detail" id="detail" type="text" class="form-control"> {{ $product->detail }}</textarea>
          </div>
        </div>

      </div>

      <div class="card-footer">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group row">
              <div class="col-sm-4"></div>
              <div class="col-sm-8">
                <button type="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;
                <a href="{{url('system/products')}}" class="btn btn-warning">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

  </div>
</section>
<!-- /.content -->
@endsection