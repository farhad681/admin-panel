@include('backend.layouts.header')
@include('backend.layouts.navbar')
@include('backend.layouts.sidebar')
@include('backend.layouts.bredcrumb')
@yield('content')
@include('backend.layouts.footer')