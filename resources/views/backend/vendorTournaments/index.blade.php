@push('css')
<style>
input.form-control.float-left.search_input {
  width: 250px;
}

ul.pagination {
  float: right;
}
</style>
@endpush
@extends("backend.master")
@section("content")
<section class="container-fluid">
  <!-- Default box -->

  <div class="col-sm-5">
    <div class="card">

      <div class="card-header mb-2">
        <h2 class="card-title"> Add Tournament </h2>
      </div>

      <div class="card-body p-2">

        <form action="{{ url('system/vendor-tournament/store') }}" method="post">
          @csrf
          {!! validation_errors($errors) !!}

          <div class="form-group row">
            <label class="col-sm-3 col-form-label" for="tournament_name">Title<code>*</code></label>
            <div class="col-sm-9">
              <input type="text" name="tournament_name" id="tournament_name" class="form-control"
                value="{{ old('tournament_name') }}" placeholder="Enter Title">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-3 col-form-label" for="tournament_name">Sport Type<code>*</code></label>
            <div class="col-sm-9">
              <select name="sport_type" id="sport_type" class="form-control">
                <option value="">Select Sport Type</option>
                @foreach($vendorCategories as $vendorCategory)
                <option value="{{ $vendorCategory->id }}">{{ $vendorCategory->description }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h2 class="card-title"> Tournament List </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">

      <div class="col-md-12 mt-4" style="overflow-y: scroll; display: block; height: 100px;">
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Action</th>
              <th class="text-center">ID</th>
              <th class="text-center">Name</th>
              <th class="text-center">Sports</th>
            </tr>
          </thead>
					<tbody>
						@if (!empty($allData))
						@foreach($allData as $key => $data)
						<tr>
							<td class="text-center">{{++$key}}</td>
							<td class="text-center">

								<a class="text-danger" id="action" data-toggle="modal" data-target="#windowmodal"
									href="{{ url('system/vendor-tournament/delete/'.$data->tournaments_id) }}"><i class="fa fa-trash"></i></a>
							</td>

							<td class="text-center">{{$data->tournaments_id}}</td>
							<td class="text-center">{{$data->tournament_name}}</td>
							<td class="text-center">
								@foreach($vendorCategories as $vendorCategory)
								{{($vendorCategory->id == $data->sport_type) ? $vendorCategory->description : ''}}
								@endforeach
							</td>
						</tr>
						@endforeach
						@endif
					</tbody>
        </table>
      </div>
    </div>
  </div>


</section>
<!-- Modal -->
<div class="modal fade" id="windowmodal" tabindex="-1" role="dialog" aria-labelledby="windowmodal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="windowmodal">&nbsp;</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="spinner-border"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection