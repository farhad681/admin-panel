
<style type="text/css">
.alert {
  padding: 6px 10px;
  margin-top: 10px
}

.alert-warning {
  display: none;
}

.alert-success {
  display: none;
}

.alert-warning ul {
  margin-bottom: 0px !important;
}
</style>

<form method="post" action="{{ url('system/vendor-tournament/update') }}" id="edit">
  @csrf
  <div class="modal-content">
    <div class="modal-header">
      <input type="hidden" class="datepickerNone">
      <h4 class="modal-title" id="myModalLabel"> Edit Tournament </h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
          aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
      <div class="card-body">
        <div id="error_message"></div>
        <div class="alert alert-warning" role="alert">&nbsp;</div>
        <div class="alert alert-success" role="alert">&nbsp;</div>
        <div class="col-md-12 fbody">

          <input type="hidden" value="{{ getValue('tournaments_id', $objData) }}" id="id" name="id">

          <div class="input-group mb-3">
            <label class="col-sm-3 col-form-label" for="tournament_name">Tournament Title<code>*</code></label>
            <input type="text" name="tournament_name" value="{{getValue('tournament_name', $objData) }}" id="tournament_name"
              class="form-control">
          </div>

          <div class="input-group mb-3">
            <label class="col-sm-3 col-form-label" for="sport_type">Sports Type<code>*</code></label>
            <input type="text" name="tournament_name" value="{{getValue('tournament_name', $objData) }}" id="tournament_name"
              class="form-control">
            <select name="sport_type" id="sport_type" class="form-control">
              @foreach($vendorCategories as $vendorCategory)
               <option {{ ($vendorCategory->id == getValue('sport_type', $objData)) ? 'selected':'' }} value="{{ $vendorCategory->id }}">{{ $vendorCategory->description }}</option>
              @endforeach
            </select>
          </div>

        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" id="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;
      <button type="button" data-reload="true" class="btn btn-secondary dismiss" data-dismiss="modal">Close</button>
    </div>
</form>


<script>
$(function() {
  $('form#edit').each(function() {
    $this = $(this);
    $this.find('#submit').on('click', function(event) {
      event.preventDefault();
      var str = $this.serialize();
      $.post('{{ url("system/vendor-tournament/update") }}', str, function(response) {
        if (response == 'success') {
          $this.find('.alert-success').html('Successfully Updated').hide().slideDown();
          $this.find('.fbody').hide();
          $('.alert-warning').hide();
        } else {
          var html = '<ul>'
          $.each(response, function(index, item) {
            html += '<li>' + item + '</li>'
          });
          html += '</ul>'
          $('.alert-warning').html(html).hide().slideDown();
          $('.alert-success').hide();
        }
      });

    });
  });
});
</script>