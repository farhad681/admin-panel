@push('css')
<style>
#tooltip {
  position: absolute;
  right: -2%;
  top: 25%;
}

#tooltip .fa {
  font-size: 14px;
  color: #666
}

.documents .tooltiptext {
  visibility: hidden;
  width: 250px;
  background-color: white;
  color: green;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.documents:hover .tooltiptext {
  visibility: visible;
}
</style>

@endpush

@extends("backend.master")
@section("content")
<!-- Main content -->
<section class="container-fluid">
  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> Add New </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool" >
          <a href="{{ url('system/vendor-category') }}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>

    <form method="post" action="{{ url('system/vendor-category/store') }}" enctype="multipart/form-data">
      @csrf

      <div class="card-body">

        {!! validation_errors($errors) !!}

        <input type="hidden" value="{{ getValue('id', $objData) }}" id="id" name="id">

        <div class="row">
          <div class="col-sm-6">

            <div class="form-group row">
              <label for="description" class="col-sm-4 col-form-label">Title <code>*</code></label>
              <div class="col-sm-8">
                <input type="text" name="description" id="description" class="form-control" value="{{ getValue('description', $objData) }}">
              </div>
            </div>

          </div>
          <div class="col-sm-6">

            <div class="form-group row">
              <label for="status" class="col-sm-4 col-form-label">Status</label>
              <div class="col-sm-8">
                <select name="status" id="status" class="form-control">
                  <option value="">Select Status</option>
                  <option value="1" {{ (getValue('status', $objData) == 1) ? 'selected': '' }}>Active</option>
                  <option value="0" {{ (getValue('status', $objData) == 0) ? 'selected': '' }}>Inactive</option>
                </select>
              </div>
            </div>

          </div>
        </div>

      </div>

      <div class="card-footer">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group row">
              <div class="col-sm-4"></div>
              <div class="col-sm-8">
                @if(empty(getValue('id', $objData)))
                <button type="submit" class="btn btn-primary">Save</button>&nbsp;&nbsp;
                @else 
                <button type="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;
                @endif 
                <a href="{{ url('system/vendor-category') }}" class="btn btn-warning">Cancel</a>
              </div> 
            </div>
          </div>
        </div>
      </div>

    </form>
  </div>
</section>
@endsection

@push('plugin')
<script src="{{url('backend/plugins/tinymce/tinymce.min.js')}}"></script>
@endpush

@push('js')

<script>
  $(function() {
    $('[data-toggle="tooltip"]').tooltip()

    $('[data-toggle="documents"]').tooltip()

  })

</script>
@endpush