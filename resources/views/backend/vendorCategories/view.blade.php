@push('css')
<style>
#tooltip {
  position: absolute;
  right: -2%;
  top: 25%;
}

#tooltip .fa {
  font-size: 14px;
  color: #666
}
</style>

@endpush

@extends("backend.master")
@section("content")
<!-- Main content -->
<section class="container-fluid">
  <!-- Default box -->

  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> Category Information </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool">
          <a href="{{ url('system/vendor-category/'.$objData->id.'/edit') }}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i>
              <i class="fa fa-edit"></i> Edit</a>
          <a href="{{ url('system/vendor-category') }}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i
                class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>

    <div class="card-body">
      <div class="col-md-11">
        <div class="row">
          <div class="col-md-6">

            <div class="row">
              <label class="col-sm-3">Title</label>
              <div class="col-sm-9">
                {{ getValue('description', $objData) }}
              </div>
            </div>

          </div>
          <div class="col-md-6">

            <div class="row">
              <label class="col-sm-3">Status</label>
              <div class="col-sm-9">
                {{ (getValue('status', $objData) == 1) ? 'Active' : 'Inactive' }}
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->
@endsection