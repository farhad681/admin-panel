@push('css')
  <style>
    input.form-control.float-left.search_input{
      width: 250px;
    }
    ul.pagination{
      float: right;
    }
  </style>
@endpush
@extends("backend.master")
@section("content")

  <section class="content">
    <!-- Default box -->
    <div class="row">

      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h2 class="card-title"> Category </h2>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>

              <button type="button" class="btn btn-tool" >
                <a href="{{url('system/vendor-category/create')}}" class="btn bg-gradient-info custom_btn"><i class="mdi mdi-plus"></i> <i class="fa fa-plus-circle"></i> Add New </a>
              </button>

            </div>
          </div>
          <div class="card-body frontoffice-body">

            <table class="table table-bordered">
              <thead>
              <tr>
                <th class="text-center">SL</th>
                <th class="text-center">Title</th>
                <th class="text-center">Status</th>
                <th class="text-center">Manage</th>
              </tr>
              </thead>
              <tbody>
              @if ($allData->count() > 0)

                @php
                  $c = 1;
                @endphp

                @foreach ($allData as $data)
                <tr>
                <td class="text-center">{{ $c }}</td>
                  <td class="text-center">{{ $data->description }}</td>
                  <td class="text-center">
                    {{ ($data->status == 1) ? 'Active' : 'Inactive' }}
                  </td>
                  <td class="text-center">
                    <div class="btn-group">

                      <button type="button" class="btn btn-outline-info">
                        <a href="{{ url('system/vendor-category/'.$data->id) }}"><i class="fa fa-table"></i> </a>
                      </button>

                      <button type="button" class="btn btn-outline-info dropdown-toggle dropdown-hover dropdown-icon"
                        data-toggle="dropdown">
                      </button>
                      <div class="dropdown-menu" role="menu" style="">
                        <a class="dropdown-item" href="{{ url('system/vendor-category/'.$data->id.'/edit') }}"><i
                            class="fa fa-edit"></i> Edit</a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" id="action" data-toggle="modal" data-target="#windowmodal"
                          href="{{ url('system/vendor-category/delete/'.$data->id) }}"><i class="fa fa-trash"></i> Delete</a>

                      </div>
                    </td>
                  </tr>

                  @php
                    $c++;
                  @endphp

                @endforeach

              @else

                <tr>
                  <td colspan="5">There is nothing found.</td>
                </tr>


              @endif
              </tbody>
            </table>
          </div>
          <div class="card-footer">
           
          </div>
        </div>

      </div>
    </div>
  </section>

  <!-- Modal -->
  <div class="modal fade" id="windowmodal" tabindex="-1" role="dialog" aria-labelledby="windowmodal"
       aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="windowmodal">&nbsp;</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="spinner-border"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!-- /.content -->
@endsection