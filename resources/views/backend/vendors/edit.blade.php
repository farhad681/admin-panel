@extends('backend.master')

@section('content')
<!-- Main content -->
<section class="container-fluid">
  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> Edit Category </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>

        <button type="button" class="btn btn-tool">
          <a href="{{url('system/categories')}}" class="btn btn-info btn-sm"><i class="mdi mdi-plus"></i> <i
              class="fa fa-arrow-left"></i> Back</a>
        </button>
      </div>
    </div>


    <form method="post" action="{{ route('categories.update',$category->id) }}" enctype="multipart/form-data">
      @method('put')
      @csrf

      <div class="card-body">

        @if ($errors->any())
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif




        <div class="form-group row" id="area_charge">
          <label class="col-sm-4 col-form-label" for="charge">Title</label>
          <div class="col-sm-8">
            <input name="title" id="title" type="text" class="form-control" value="{{ $category->title }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label" for="description">Details </label>
          <div class="col-sm-8">
            <textarea name="description" id="description" type="text" class="form-control"> {{ $category->description }}</textarea>
          </div>
        </div>

      </div>

      <div class="card-footer">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group row">
              <div class="col-sm-4"></div>
              <div class="col-sm-8">
                <button type="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;
                <a href="{{url('system/categories')}}" class="btn btn-warning">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

  </div>
</section>
<!-- /.content -->
@endsection