
@extends("backend.master")
@section("content")
<!-- Main content -->
<section class="container-fluid">
  <div class="card">

    <div class="card-header">
      <h2 class="card-title"> Add New </h2>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>

    <form method="post" action="{{url($bUrl.'/store')}}" enctype="multipart/form-data">
      @csrf

      <div class="card-body">

      {!! validation_errors($errors) !!}

        <input type="hidden" value="{{ getValue($tableID, $objData) }}" id="id" name="{{ $tableID }}">

        <div class="row">
          <label for="first_team" class="col-sm-6 col-form-label">First Team <code>*</code></label>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <input type="text" name="first_team" id="first_team" class="form-control" value="{{ getValue('first_team', $objData) }}">
          </div>
        </div>

        <div class="row">
          <label for="second_team" class="col-sm-6 col-form-label">Second Team</label>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <input type="text" name="second_team" id="second_team" class="form-control" value="{{ getValue('second_team', $objData) }}">
          </div>
        </div>

        @php 
          if(! empty(getValue('vendor_datetime', $objData))) {
            $dateTime = getValue('vendor_datetime', $objData);
            $date = substr($dateTime, 0, 10);
            $time = substr($dateTime, 11, 15);
          } else {
            $date = "";
            $time = "";
          }
        @endphp 

        <div class="row">
          <label for="match_date" class="col-sm-6 col-form-label">Match Date</label>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <input type="text" name="match_date" id="match_date" class="form-control dateOfBirth" value="{{ $date }}">
          </div>
        </div>

        <div class="row">
          <label for="match_time" class="col-sm-6 col-form-label">Match Time</label>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <input name="match_time" id="match_time" type="text" class="form-control timePicker" value="{{ $time }}"/>
          </div>
        </div>

        <div class="row">
          <label for="tournament" class="col-sm-6 col-form-label">Tournament</label>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <select name="tournament" id="tournament" class="form-control">
              <option value="">Select Tournament</option>
              @foreach($vendorTorunaments as $vendorTorunament)
                <option {{ ($vendorTorunament->tournaments_id == getValue('tournament', $objData)) ? 'selected' : '' }} value="{{ $vendorTorunament->tournaments_id  }}">{{ $vendorTorunament->tournament_name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <label for="cat_id" class="col-sm-6 col-form-label">Sport Type</label>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <select name="cat_id" id="cat_id" class="form-control">
              <option value="">Select Sport Type</option>
              @foreach($vendorCategories as $vendorCategory)
                <option {{ ($vendorCategory->id == getValue('cat_id', $objData)) ? 'selected' : '' }} value="{{ $vendorCategory->id  }}">{{ $vendorCategory->description }}</option>
              @endforeach
            </select>
          </div>
        </div>

      </div>

      <div class="card-footer">
        <div class="row">
          <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-4"></div>
              <div class="col-sm-8">
                <button type="submit" class="btn btn-primary">Save</button>&nbsp;&nbsp;
                <a href="{{url('system/categories')}}" class="btn btn-warning">Cancel</a>
              </div> 
            </div>
          </div>
        </div>
      </div>

    </form>
  </div>
</section>
@endsection