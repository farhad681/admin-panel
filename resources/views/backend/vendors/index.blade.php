@push('css')
  <style>
    input.form-control.float-left.search_input{
      width: 250px;
    }
    ul.pagination{
      float: right;
    }
  </style>
@endpush
@extends("backend.master")
@section("content")

  <section class="content">
    <!-- Default box -->
    <div class="row">

      <div class="col-12">
        <div class="card">
          <div class="card-header">
            @foreach($vendorCategories as $vendorCategory)
              <a href="{{ url($bUrl.'/'.$vendorCategory->id) }}" @if($vendorCategory->id == $category) class="btn btn-success" @else class="btn btn-info" @endif >{{ $vendorCategory->description }}</a>&nbsp;&nbsp;&nbsp;&nbsp;
            @endforeach
          </div>
          <div class="card-body frontoffice-body">
            
            <table class="table table-bordered">
              <thead>
              <tr>
                <th class="text-center" width="5%">SL</th>
                <th class="text-left" width="15%">Action</th>
                <th class="text-left" width="10%">ID</th>
                <th class="text-left" width="30%">Team</th>
                <th class="text-left" width="20%">Tour</th>
                <th class="text-left" width="10%">Status</th>
                <th class="text-left" width="10%">Match Time</th>
              </tr>
              </thead>
              <tbody>
                @foreach($vendors as $key => $vendor)
                  <tr>
                    <td class="text-center" width="5%">{{ ++$key }}</td>
                    <td class="text-left" width="15%">
                      <a class="text-primary" href="{{url($bUrl.'/'.$vendor->vendor_id.'/edit')}}"><i class="fa fa-pen"></i></a>
                      <a class="text-danger" id="action" data-toggle="modal" data-target="#windowmodal"
                        href="{{url($bUrl.'/delete/'.$vendor->vendor_id)}}"><i class="fa fa-trash"></i></a>
                    </td>
                    <td class="text-left" width="10%">{{ $vendor->vendor_id }}</td>
                    <td class="text-left" width="30%">{{ $vendor->first_team }} vs {{ $vendor->second_team }}</td>
                    <td class="text-left" width="20%">{{ $vendor->tournament_name }}</td>
                    <td class="text-left" width="10%">
                      {{ ($vendor->status == 1) ? 'Active' : 'Draft' }}
                    </td>
                    <td class="text-left" width="10%">{{ $vendor->vendor_datetime }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>

          </div>
        </div>

      </div>
    </div>
  </section>

  <!-- Modal -->
<div class="modal fade" id="windowmodal" tabindex="-1" role="dialog" aria-labelledby="windowmodal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="windowmodal">&nbsp;</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="spinner-border"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection